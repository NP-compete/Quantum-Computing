# Quantum-Computing
Notes for Quantum Computing and Cryptography

**Index:**

- Notebooks starting with **P** are practicals

- Notebooks starting with **T** are theory

- Notebooks starting with **B** are sections of the book which are of not much use.